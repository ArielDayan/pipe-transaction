export default {
    retryOptions: {
        retries: 3,
        factor: 1.5
    },
    showLogs: true,
    autoIdGenerate: false,
    continueOnUndoError: true,
    packageName: 'pipe-transaction'
}